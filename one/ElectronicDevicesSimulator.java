public class ElectronicDevicesSimulator{
    public static void main(String[] args){
        // Command[] onCommands = new Command[4];
        // Command[] offCommands = new Command[4];
        //Forloop command and set command

        //Create all component of SmartTV
        PowerSupply powerSupply = new PowerSupply();
        Motherboard motherboard = new Motherboard();
        LCDScreen lcdScreen = new LCDScreen();
        SmartTV smartTV = new SmartTV(lcdScreen, motherboard, powerSupply);

        //Turn all component of SmartTV on to switch the TV on
        powerSupply.switchOn();

        //Turn all component of SmartTV off to switch the TV off
        smartTV.switchOff();
    }
}

class SmartTV {

    private LCDScreen lcdScreen;
    private Motherboard motherboard;
    private PowerSupply powerSupply;

    public SmartTV(LCDScreen lcdScreen, Motherboard motherboard, PowerSupply powerSupply){
        this.lcdScreen = lcdScreen;
        this.motherboard = motherboard;
        this.powerSupply = powerSupply;
    }

    public void switchOn() {
        System.out.println("Smart TV is On");
        powerSupply.switchOn();
        motherboard.switchOn();
        lcdScreen.switchOn();
    }

    public void switchOff() {
        System.out.println("Smart TV is Off");
        lcdScreen.switchOff();
        motherboard.switchOff();
        powerSupply.switchOff();
    }

}
class LCDScreen {
    public void switchOn() {
        System.out.println("LCD Screen is On");
    }

    public void switchOff() {
        System.out.println("LCD Screen is Off");
    }
}

class PowerSupply {
    public void switchOn() {
        System.out.println("Power Supply is On");
    }

    public void switchOff() {
        System.out.println("Power Supply is Off");
    }
}

class Motherboard {
    public void switchOn() {
        System.out.println("Motherboard is On");
    }

    public void switchOff() {
        System.out.println("Motherboard is Off");
    }
}

interface Command {
    public void execute();
}

// class LCDScreenOn implements Command {
//     LCDScreen lcdScreen;
//     public LCDScreenOn(LCDScreen lcdScreen) {
//         this.lcdScreen = lcdScreen;
//     }
//     public void execute() {
//         lcdScreen.switchOff();
//     }
// }

// class LCDScreenOff implements Command {
//     LCDScreen lcdScreen;
//     public LCDScreenOff(LCDScreen lcdScreen) {
//         this.lcdScreen = lcdScreen;
//     }
//     public void execute() {
//         lcdScreen.switchOn();
//     }
// }

// class PowerSupplyOff implements Command {
//     PowerSupply powerSupply;
//     public PowerSupplyOff(PowerSupply powerSupply) {
//         this.powerSupply = powerSupply;
//     }
//     public void execute() {
//         powerSupply.switchOff();
//     }
// }

// class PowerSupplyOn implements Command {
//     PowerSupply powerSupply;
//     public PowerSupplyOn(PowerSupply powerSupply) {
//         this.powerSupply = powerSupply;
//     }
//     public void execute() {
//         powerSupply.switchOff();
//     }
// }


// class MotherboardOff implements Command {
//     Motherboard motherboard;
//     public MotherboardOff(Motherboard motherboard) {
//         this.motherboard = motherboard;
//     }
//     public void execute() {
//         motherboard.switchOff();
//     }
// }

// class MotherboardOn implements Command {
//     Motherboard motherboard;
//     public MotherboardOn(Motherboard motherboard) {
//         this.motherboard = motherboard;
//     }
//     public void execute() {
//         motherboard.switchOff();
//     }
// }

// class SmartTVOff implements Command {
//     SmartTV smartTV;
//     public SmartTVOff(SmartTV smartTV) {
//         this.smartTV = smartTV;
//     }
//     public void execute() {
//         smartTV.switchOff();
//     }
// }

// class SmartTVOn implements Command {
//     SmartTV smartTV;
//     public SmartTVOn(SmartTV smartTV) {
//         this.smartTV = smartTV;
//     }
//     public void execute() {
//         smartTV.switchOff();
//     }
// }